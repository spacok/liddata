#include <math.h>
#include <stdio.h>
#include "vector3d.h"

void vector3d_init(vector3d *vec, double x, double y, double z)
{
    vec->x = x;
    vec->y = y;
    vec->z = z;
}



double vector3d_length(vector3d vec)
{
    return (sqrt(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z));
}

vector3d vector3d_normalize(vector3d vec)
{
    vector3d result;
    double length = vector3d_length(vec);
    result.x = vec.x / length;
    result.y = vec.y / length;
    result.z = vec.z / length;
    return result;
}

vector3d vector3d_crossproduct(vector3d vec1, vector3d vec2)
{
    vector3d cross;
    cross.x = vec1.y * vec2.z - vec2.y * vec1.z;
    cross.y = vec1.z * vec2.x - vec2.z * vec1.x;
    cross.z = vec1.x * vec2.y - vec2.z * vec1.y;
    return cross;
}

double vector3d_angle(vector3d vec1, vector3d vec2)
{
    vector3d norm1 = vector3d_normalize(vec1);
    vector3d norm2 = vector3d_normalize(vec2);
    double dotproduct = vector3d_dotproduct(norm1, norm2); 
    double angle = acos(dotproduct) * 180 / M_PI;
    if (angle == 0 || angle == 180)
        return angle;
    vector3d axis = vector3d_normalize(vector3d_crossproduct(norm1, norm2));
    if (axis.z < 0)
        angle = 360 - angle;
    return (angle);
}

double vector3d_dotproduct(vector3d vec1, vector3d vec2)
{
    return (vec1.x * vec2.x + vec1.y * vec2.y + vec1.z * vec2.z);
}


