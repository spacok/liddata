#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>


#include "lidangle.h"
#include "loop.h"
#include "vector3d.h"
#include "dbus.h"

#define MAX_STR_SIZE 512
#define TIMER_VALUE 500

const unsigned char kCordNames[3] = {'x','y','z'}; 
static int timer_id = INVALID_TIMER_ID;
const float kMeanGravity = 9.80665f;


static int read_toInt(int fd)
{
    char val[MAX_STR_SIZE];
    memset (val, 0, MAX_STR_SIZE);
    int rd = read(fd, val, MAX_STR_SIZE);
    if (rd == 0)
        fprintf(stderr, "Nothing read\n");
    lseek(fd, 0, SEEK_SET);
    return atoi(val);
}

static double read_toDouble(int fd)
{
    char val[MAX_STR_SIZE];
    memset (val, 0, MAX_STR_SIZE);
    int rd = read(fd, val, MAX_STR_SIZE);
    if (rd == 0)
        fprintf(stderr, "Nothing read\n");
    printf("Value: %s\n",val);
    lseek(fd, 0, SEEK_SET);
    return strtod(val,NULL);
}

static rotation_t lidangle_rotationFromAngle(double angle)
{
    if (angle >= 0 && angle < 45)
    {
        return bottom;
    }
    else if (angle >= 45 && angle < 135)
    {
        return left;
    }
    else if (angle >= 135 && angle < 225)
    {
        return top;
    }
    else if (angle >= 225 && angle < 315)
    {
        return right;
    }
    return bottom;
}

static void lidangle_read_callback(int timer_id, void *data)
{
    int i;
    struct lidangle_data_t *lid = data;
    vector3d vec1;
    vector3d vec2;
    loop_stop_timer(timer_id);
    for (i = 0; i < 3; i++)
    {
	    lid->coords[i] = read_toInt(lid->file_lid[i]);
    }
    //Only 2d vector is necessary
    vector3d_init(&vec1, lid->coords[0] * lid->scale, lid->coords[1] * lid->scale, 0);
    vector3d_init(&vec2, 0, 1, 0);
    rotation_t rot = lidangle_rotationFromAngle(vector3d_angle(vec1, vec2));
    if (lid->rotation != rot)
    {
        if (lid->rotation != undefined)
        {
            bool ret = dbus_sendsignal("EV_SW", 2, (int) rot);
            if (ret == false)
            {
                printf("Failed to send\n");
            }
        }
        lid->rotation = rot;
    }
    loop_start_timer(timer_id, TIMER_VALUE);
}

static int open_lidangle_sysfs( const char* const path )
{
    printf("Opening %s\n", path);
    return open(path, O_RDONLY);
}

bool lidangle_init(struct lidangle_data_t* const lid, const char* const lidsysfs_path)
{
    int i;
    lid->lidsysfs_path = lidsysfs_path;
    char filename [512];
    for (i = 0; i < 3; i++)
    {
        sprintf(filename,"%s/in_accel_%c_raw", lid->lidsysfs_path, kCordNames[i]);
        lid->file_lid[i] = open_lidangle_sysfs(filename);
        if(0 > lid->file_lid[i])
        {
            perror("ERROR opening LID event file");
            return false;
        }
    }
    sprintf(filename,"%s/scale", lid->lidsysfs_path);
    int fd = open_lidangle_sysfs(filename);
    if (0 > fd)
    {
        perror("ERROR opening scale file, scale will not be used");
        lid->scale = 1.0f;
    }
    else
    {
        lid->scale = read_toDouble(fd);
        if (lid->scale == 0)
            lid->scale = 1.0f;
    }
    lid->scale = 1.0;
    lid->rotation = undefined;
    timer_id = loop_add_timer(&lidangle_read_callback, lid);

    if(timer_id == INVALID_TIMER_ID)
    {
        fprintf(stderr, "Error adding sysfs timer\n");
        return false;
    }
    lidangle_read_callback(timer_id, lid);
    return true;
}

void lidangle_free(struct lidangle_data_t* const lid)
{
    int i;
    for (i = 0; i < 3; i++)
    {
        if(0 != close(lid->file_lid[i]))
        {
            perror("ERROR closing LID event file\n");
        }
    }
    memset(lid, 0, sizeof(*lid));
}
