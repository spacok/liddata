#include "lidevent.h"

#include <linux/input.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "loop.h"
#include "dbus.h"

static int open_lid_event( const char* const path )
{
	printf("Opening");
    return open(path, O_RDONLY);
}

static void lid_read_callback(void* data)
{
	struct lid_data_t* const lid = data;
	struct input_event ev[64];
	int rd, i;
	rd = read(lid->file_lid, ev, sizeof(struct input_event) * 64);

	if(rd < sizeof(struct input_event))
	{
		// TODO handle this? We get this ad infinitum when we pull the reader out
		// We'd need to remove the file from the poll loop, close it, re-open and
		// re-add.
		//perror("Error reading from file");
		fprintf(stderr, ".");
	}
	else
	{
		for (i = 0; i < rd / sizeof(struct input_event); i++)
		{
			if (ev[i].type == EV_SW)
			{
				bool ret = dbus_sendsignal("EV_SW", ev[i].code, ev[i].value);
				if (ret == false)
				{
					printf("Failed to send\n");
				}
			}
		}
	}

}

bool lid_init(struct lid_data_t* const lid, const char* const lidevent_path)
{
    memset(lid, 0, sizeof(struct lid_data_t));
    lid->lidevent_path = lidevent_path;
    lid->file_lid = open_lid_event(lidevent_path);
    if(0 > lid->file_lid)
    {
        perror("ERROR opening LID event file");
        return false;
    }
    if(!loop_add_fd(lid->file_lid, &lid_read_callback, lid))
    {
        fprintf(stderr, "Error adding file to poll loop\n");
        return false;
    }
    return true;
}

void lid_free(struct lid_data_t* const lid)
{
    if(0 != close(lid->file_lid))
    {
        perror("ERROR closing LID event file\n");
    }
    memset(lid, 0, sizeof(*lid));
}
