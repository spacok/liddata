#ifndef LIDEVENT_H
#define LIDEVENT_H

#include <stdbool.h>

struct lid_data_t
{
    /// Filename of the input (/dev/input/eventX) file.
    const char* lidevent_path;
    /// File descriptor of the input file.
    int file_lid;

    /// ID read from the reader.
    char code;
    /// Length of used data in id.
    int id;
};

bool lid_init(struct lid_data_t* const lid, const char* const lidevent_path);
void lid_free(struct lid_data_t* const lid);

#endif //LIDEVENT_H
