#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <linux/limits.h>
#include <limits.h>

#include "config.h"
#include "loop.h"
#include "lidevent.h"
#include "lidangle.h"
#include "dbus.h"

#include <syslog.h>

#define CONNECTING_BLINK_TIME 500
#define WIFI_BUTTON_KEEP_CONNECTED_TIME 5

// QUICK HACK: these globals should not be globals.

static struct sigaction sigint_old_action;
static struct sigaction sigterm_old_action;
static void signal_handler(int sig_no)
{
    sigaction(SIGINT, &sigint_old_action, NULL);
    sigaction(SIGTERM, &sigterm_old_action, NULL);
    loop_stop();
}

int main(int argc, char** argv)
{
    int ret = 0;
	static struct config_t config;

    struct sigaction action;
    memset(&action, 0, sizeof(action));
    action.sa_handler = &signal_handler;
    sigaction(SIGINT, &action, &sigint_old_action);
    sigaction(SIGTERM, &action, &sigterm_old_action);

    /* default path in home, overridable by first CLI arg */
    const char* config_path = "liddata.cfg";
    if (argc >= 2)
    {
        config_path=argv[1];
    }

    if (!config_init(&config, config_path))
    {
        return 1;
    }
	if (!dbus_init(config.dbus_destination))
	{
		return 1;
	}

    struct lid_data_t lid;
    struct lidangle_data_t lidangle;

    if (!lid_init(&lid, config.lidevent_path))
    {
        fprintf(stderr, "Failed to open lid input device.\n");
        ret = 9;
        goto error_lid;
    }
    if (!lidangle_init(&lidangle, config.lidsysfs_path))
    {
        fprintf(stderr, "Failed to open lid sysfs devices.\n");
        ret = 9;
        goto error_lid;
    }
	loop_run();
    error_lidsysfs:
        lidangle_free(&lidangle);
    error_lid:
        lid_free(&lid);
    return ret;
}

