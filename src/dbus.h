#ifndef __MY_DBUS_H__
#define __MY_DBUS_H__

#include <stdbool.h>

bool dbus_sendsignal(const char* const EV_TYPE, unsigned int EV_CODE, unsigned int EV_VALUE);
bool dbus_init(const char* const destination);
#endif

