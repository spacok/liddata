#ifndef VECTOR3D_H
#define VECTOR3D_H

typedef struct
{
    double x,y,z;
} vector3d;

void vector3d_init(vector3d *vec, double x, double y, double z);
double vector3d_length(vector3d vec);
double vector3d_angle(vector3d vec1, vector3d vec2);
double vector3d_dotproduct(vector3d vec1, vector3d vec2); 

#endif
