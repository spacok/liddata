#ifndef CONFIG_H_KO8K9RVM
#define CONFIG_H_KO8K9RVM

#include <stdbool.h>

#define CONFIG_MAX_STR_SIZE 512

struct config_t
{
    char lidevent_path[CONFIG_MAX_STR_SIZE];
    char lidsysfs_path[CONFIG_MAX_STR_SIZE];
	char dbus_destination[CONFIG_MAX_STR_SIZE];
};

bool config_init( struct config_t* const cfg, const char* const config_path );

#endif /* end of include guard: CONFIG_H_KO8K9RVM */
