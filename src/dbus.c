/*
 * Example low-level D-Bus code.
 * Written by Matthew Johnson <dbus@matthew.ath.cx>
 *
 * This code has been released into the Public Domain.
 * You may do whatever you like with it.
 *
 * Subsequent tweaks by Will Ware <wware@alum.mit.edu>
 * Still in the public domain.
 */
#include <dbus/dbus.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "dbus.h"

struct dbus_iface_t
{
	const char *dest;
	DBusConnection* conn;
	DBusError err;
};

struct dbus_iface_t dbus_iface;
/**
 * Connect to the DBUS bus and send a broadcast signal
 */

bool dbus_sendsignal(const char* const EV_TYPE, unsigned int EV_CODE, unsigned int EV_VALUE)
{
	DBusMessage* msg;
	DBusMessageIter args;
	int ret;
	dbus_uint32_t serial = 0;

	// create a signal & check for errors
	msg = dbus_message_new_signal("/lid/daemon/Event", // object name of the signal
			"lid.daemon.Event", // interface name of the signal
			"LidEvent"); // name of the signal
	if (NULL == msg)
	{
		fprintf(stderr, "Message Null\n");
		return(false);
	}
	if (!dbus_message_set_destination(msg, "org.awesomewm.awful"))
		printf("Can not set destination\n");
	// append arguments onto signal
	dbus_message_iter_init_append(msg, &args);
	if (!dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &EV_TYPE)) {
		fprintf(stderr, "Out Of Memory!\n");
		return(false);
	}
	if (!dbus_message_iter_append_basic(&args, DBUS_TYPE_UINT32, &EV_CODE)) {
		fprintf(stderr, "Out Of Memory!\n");
		return(false);
	}
	if (!dbus_message_iter_append_basic(&args, DBUS_TYPE_UINT32, &EV_VALUE)) {
		fprintf(stderr, "Out Of Memory!\n");
		return(false);
	}
	// send the message and flush the connection
	if (!dbus_connection_send(dbus_iface.conn, msg, &serial)) {
		fprintf(stderr, "Out Of Memory!\n");
		return(false);
	}
	dbus_connection_flush(dbus_iface.conn);
	// free the message
	dbus_message_unref(msg);
	return (true);
}

bool dbus_init(const char* const destination)
{
	int ret;
	dbus_iface.dest = destination;
	dbus_iface.conn = dbus_bus_get(DBUS_BUS_SESSION, &dbus_iface.err);
	if (dbus_error_is_set(&dbus_iface.err)) {
		fprintf(stderr, "Connection Error (%s)\n", dbus_iface.err.message);
		dbus_error_free(&dbus_iface.err);
	}
	if (NULL == dbus_iface.conn) {
		return (false);
	}
	// register our name on the bus, and check for errors
	ret = dbus_bus_request_name(dbus_iface.conn, "lid.daemon.source", DBUS_NAME_FLAG_REPLACE_EXISTING , &dbus_iface.err);
	if (dbus_error_is_set(&dbus_iface.err)) {
		fprintf(stderr, "Name Error (%s)\n", dbus_iface.err.message);
		dbus_error_free(&dbus_iface.err);
	}
	if (DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER != ret) {
		return (false);
	}
	
	return (true);
}

