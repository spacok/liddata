#ifndef LIDANGLE_H
#define LIDANGLE_H
#include <stdbool.h>

typedef enum 
{
    top,
    left,
    right,
    bottom,
    undefined,
}rotation_t;

struct lidangle_data_t
{
    const char* lidsysfs_path;
    int file_lid[3];
    int coords[3];
    double scale;
    rotation_t rotation;
};

bool lidangle_init(struct lidangle_data_t* const lid, const char* const lidsysfs_path);
void lidangle_free(struct lidangle_data_t* const lid);

#endif
